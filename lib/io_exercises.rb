# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


def guessing_game
  random_number = Random.new.rand(1..100)
  counter = 0
  correct_or_nah = false
  until correct_or_nah
    puts "Guess a number 1 to 100"
    guess = gets.chomp
    if guess.to_i < random_number
      puts "#{guess} is too low"
    elsif guess.to_i > random_number
      puts "#{guess} is too high"
    else
      correct_or_nah = true
    end
    counter += 1
  end

  puts "#{guess} was correct! It took you #{counter} guesses"

end


def file_shuffler(file_name)
  base = File.basename(file_name, ".*")
  extension = File.extname(file_name)
  File.open("#{base}-shuffled#{extension}", "w") do |f|
    File.readlines(file_name).shuffle.each do |line|
      f.puts line.chomp
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  if ARGV.length == 1
    shuffle_file(ARGV.shift)
  else
    puts "ENTER FILENAME TO SHUFFLE:"
    filename = gets.chomp
    shuffle_file(filename)
  end
end
# if __FILE__ == $PROGRAM_NAME
#   guessing_game
# end
